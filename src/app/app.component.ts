import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Coin Counter';
  total:number = 0;


  increment(val){
    this.total += val/100;
  }
}
